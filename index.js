// Load the expressjs module into our application and saved it in a variable called express
// syntax: require("package");

const express = require("express");

// localhost port number
const port = 4000;

// app is our server
// create an application that uses express and stores it as app
const app = express();

// middleware
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// mock data
let users = [
    {
        username: "TStrak3000",
        email: "starkIndustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "ThorThunder",
        email: "loveAndThunder@mail.com",
        password: "iLoveStormBreaker"
    }
];

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
];


// Express has methods to use as routes correspoding to HTTP methods
// Syntax: app.method(<endpoint>, <function for req and res>)


app.get('/', (req, res)=>{
    // res.send() actually combines writeHead() and end()
    res.send('Hello from my first expressJS API')
    // res.status(200).send("message");
});

app.get('/greeting', (req, res)=>{

    res.send('Hello from Batch203-Pineda #iloveExpressJS')
    
});

// retrieval of the users in mock database
app.get('/users', (req, res)=>{
    // res.send already stringifies for you
    res.send(users);
});


// ADDING of new user
app.post('/users', (req, res) => {

    console.log(req.body);  // result: {} empty object

    // simulate creating a new user document
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    };

    // adding newUser to the existing array
    users.push(newUser);
    console.log(users);

    // send the updated users array in the client
    res.send(users);

});


// DELETE method
    app.delete('/users', (req, res) => {
        users.pop();
        res.send(users);
    });




// PUT Method
// update user's password
// :index - wildcard
// url: localhost:4000/users/0
app.put('/users/:index', (req, res) => {
    console.log(req.body); // updated password
    // result: {}

    // an object that contains the value of URL params
    console.log(req.params); // refer to the wildcard in the endpoint
    // result: {index: 0}

    // parseInt the value of the number coming from req.params
    // ['0'] turns into [0]
    let index = parseInt(req.params.index);

    // users[0].password
        // the update will be coming from the request body
    users[index].password = req.body.password;

    // send the update user
    res.send(users[index]);
});


// GET ALL ITEMS

app.get('/items', (req, res) => {
    res.send(items);
});

// CREATE ITEM

app.post('/items', (req, res) => {
    let newItem = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    };

    items.push(newItem);
    console.log(items);
    res.send(items);
});

// UPDATE ITEM

app.put('/items/:index', (req, res) => {
    console.log(req.body); 
  
    console.log(req.params); 

    let index = parseInt(req.params.index);
    items[index].price = req.body.price;
    res.send(items[index]);
});



// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));